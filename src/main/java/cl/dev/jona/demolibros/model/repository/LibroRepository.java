package cl.dev.jona.demolibros.model.repository;

import cl.dev.jona.demolibros.model.entity.Libro;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

@Service
@Transactional
public interface LibroRepository extends JpaRepository<Libro,Long> {
}
