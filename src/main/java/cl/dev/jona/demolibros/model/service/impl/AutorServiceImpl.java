package cl.dev.jona.demolibros.model.service.impl;

import cl.dev.jona.demolibros.common.exception.AutorException;
import cl.dev.jona.demolibros.common.util.AutorConstant;
import cl.dev.jona.demolibros.model.entity.Autor;
import cl.dev.jona.demolibros.model.repository.AutorRepository;
import cl.dev.jona.demolibros.model.service.IAutorService;
import cl.dev.jona.demolibros.common.util.AutorGeneratorReport;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;


@Service
public class AutorServiceImpl implements IAutorService {

    @Autowired
    private AutorRepository autorRepository;

    @Autowired
    private AutorGeneratorReport autorGeneratorReport;

    @Override
    public List<Autor> findAll() {
        return autorRepository.findAll();
    }

    @Override
    public List<Autor> findByNombreStartingWithIgnoreCase(String palabra) {
        return autorRepository.findByNombreStartingWithIgnoreCase(palabra);
    }

    @Override
    public Optional<Autor> findById(Long id) {
        if (autorRepository.findById(id).isEmpty()){
            throw new AutorException(HttpStatus.NOT_FOUND, String.format(AutorConstant.AUTOR_NOT_FOUND_MESSAGE_ERROR, id));
        }
        return autorRepository.findById(id);
    }

    @Override
    public Autor save(Autor autor) {
        return autorRepository.save(autor);
    }

    @Override
    public byte[] exportPdf() throws JRException, FileNotFoundException {
        return autorGeneratorReport.exportToPdf(autorRepository.findAll());
    }

    @Override
    public byte[] exportXls() throws JRException, FileNotFoundException {
        return autorGeneratorReport.exportToXls(autorRepository.findAll());
    }

    @Override
    public byte[] exportXlsx() throws IOException {
        return autorGeneratorReport.exportToXlsx(autorRepository.findAll());
    }

}
