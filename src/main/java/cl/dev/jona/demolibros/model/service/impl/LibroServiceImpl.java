package cl.dev.jona.demolibros.model.service.impl;

import cl.dev.jona.demolibros.common.exception.LibroException;
import cl.dev.jona.demolibros.common.util.LibroConstant;
import cl.dev.jona.demolibros.model.entity.Libro;
import cl.dev.jona.demolibros.model.repository.LibroRepository;
import cl.dev.jona.demolibros.model.service.ILibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class LibroServiceImpl implements ILibroService {

    @Autowired
    private LibroRepository libroRepository;

    /**
     *
     * @return listado de libros registrados en la base de datos.
     */
    @Override
    public List<Libro> listarLibros() {
        return libroRepository.findAll();
    }

    /**
     * @param id_libro del libro que se desea buscar.
     * @return el libro predeterminado.
     */
    @Override
    public Optional<Libro> findById(Long id_libro) {
        if(libroRepository.findById(id_libro).isEmpty()){
            throw new LibroException(HttpStatus.NOT_FOUND, String.format(LibroConstant.LIBRO_NOT_FOUND_MESSAGE_ERROR, id_libro));
        }
         return libroRepository.findById(id_libro);
    }
}
