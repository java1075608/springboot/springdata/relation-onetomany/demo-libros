package cl.dev.jona.demolibros.model.service;


import cl.dev.jona.demolibros.model.entity.Autor;
import net.sf.jasperreports.engine.JRException;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;


public interface IAutorService {
    List<Autor> findAll();
    List<Autor> findByNombreStartingWithIgnoreCase(String palabra);
    Optional<Autor> findById(Long id);
    Autor save(Autor autor);

    byte[] exportPdf() throws JRException, FileNotFoundException;
    byte[] exportXls() throws JRException, FileNotFoundException;

    byte[] exportXlsx() throws IOException;



}
