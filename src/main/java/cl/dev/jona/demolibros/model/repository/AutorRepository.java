package cl.dev.jona.demolibros.model.repository;


import cl.dev.jona.demolibros.model.entity.Autor;
import jakarta.transaction.Transactional;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Transactional
public interface AutorRepository extends JpaRepository<Autor, Long> {
   List<Autor> findByNombreStartingWithIgnoreCase(String primeraPalabra);

}
