package cl.dev.jona.demolibros.model.entity;


import cl.dev.jona.demolibros.model.entity.Autor;
import com.fasterxml.jackson.annotation.JsonProperty;
import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;

@Entity
@Table(name = "libros")
public class Libro {
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_libro", nullable = false)
    @Id
    private Long id_libro;
    @NotBlank
    private String nombre;
    @NotBlank
    private String edicion;
    @NotNull
    private int agnoLanzamiento;

    @ManyToOne(fetch = FetchType.LAZY)
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    @JoinColumn(name = "id_autor") //crea una columna en la entidad libro donde guarda el id del autor.
    private Autor autor;

    public Autor getAutor() {
        return autor;
    }

    public void setAutor(Autor autor) {
        this.autor = autor;
    }

    public Libro(){

    }

    public Libro(String nombre, String edicion, int agnoLanzamiento) {
        this.nombre = nombre;
        this.edicion = edicion;
        this.agnoLanzamiento = agnoLanzamiento;
    }

    public Long getId_libro() {
        return id_libro;
    }

    public void setId_libro(Long id_libro) {
        this.id_libro = id_libro;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdicion() {
        return edicion;
    }

    public void setEdicion(String edicion) {
        this.edicion = edicion;
    }

    public int getAgnoLanzamiento() {
        return agnoLanzamiento;
    }

    public void setAgnoLanzamiento(int agnoLanzamiento) {
        this.agnoLanzamiento = agnoLanzamiento;
    }
}
