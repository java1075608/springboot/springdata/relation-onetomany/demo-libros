package cl.dev.jona.demolibros.model.service;

import cl.dev.jona.demolibros.model.entity.Libro;

import java.util.List;
import java.util.Optional;

public interface ILibroService {
    List<Libro> listarLibros();
    Optional<Libro> findById(Long id_autor);
}
