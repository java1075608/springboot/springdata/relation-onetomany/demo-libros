package cl.dev.jona.demolibros.model.entity;


import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;

import java.util.Date;
import java.util.List;

@Entity
@Table(name = "autores")
public class Autor {
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    @Column(name = "id_autor", nullable = false)
    @Id
    private Long id_autor;

    @NotBlank
    private String nombre;

    @NotBlank
    private String URL_imagen;


    @NotBlank
    private String nacionalidad;

    private Date fecha_nacimiento;


    @Email
    private String correo;

    /*
    OneToMany: Un autor puede tener muchos libros.
    fetch: para que no se carguen todos nuestros registros al realizar una consulta.
    mappedBy: el atributo <libros> va a estar relacionado con el atributo <autor>.
    cascade: persista a una única tabla padre
   */
    @OneToMany(fetch = FetchType.LAZY, mappedBy = "autor", cascade = CascadeType.ALL)
    private List<Libro> libros;

    public Autor() {

    }

    public Autor(String nombre, String correo, String nacionalidad, Date fecha_nacimiento, String URL_imagen) {
        this.nombre = nombre;
        this.correo = correo;
        this.nacionalidad = nacionalidad;
        this.fecha_nacimiento = fecha_nacimiento;
        this.URL_imagen = URL_imagen;
    }

    public Long getId_autor() {
        return id_autor;
    }

    public void setId_autor(Long id_autor) {
        this.id_autor = id_autor;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getURL_imagen() {
        return URL_imagen;
    }

    public void setURL_imagen(String URL_imagen) {
        this.URL_imagen = URL_imagen;
    }

    public Date getFecha_nacimiento() {
        return fecha_nacimiento;
    }

    public void setFecha_nacimiento(Date fecha_nacimiento) {
        this.fecha_nacimiento = fecha_nacimiento;
    }

    public String getNacionalidad() {
        return nacionalidad;
    }

    public void setNacionalidad(String nacionalidad) {
        this.nacionalidad = nacionalidad;
    }

    public List<Libro> getLibros() {
        return libros;
    }

    public void setLibros(List<Libro> libros) {
        this.libros = libros;
        for (Libro libro : libros) {
            libro.setAutor(this);
        }
    }
}
