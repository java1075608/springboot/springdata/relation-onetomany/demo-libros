package cl.dev.jona.demolibros.common.exception;

import org.springframework.http.HttpStatus;

public class EntityException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    private HttpStatus errorCode;
    private String errorMessage;
    private String entityName;

    public EntityException(String entityName, HttpStatus errorCode, String errorMessage) {
        this.entityName = entityName;
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
    }

    public EntityException() {
    }

    public HttpStatus getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(HttpStatus errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getEntityName() {
        return entityName;
    }

    public void setEntityName(String entityName) {
        this.entityName = entityName;
    }
}

