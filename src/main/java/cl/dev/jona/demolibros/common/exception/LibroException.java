package cl.dev.jona.demolibros.common.exception;

import org.springframework.http.HttpStatus;

import org.springframework.http.HttpStatus;

public class LibroException extends EntityException {
    private static final long serialVersionUID = 1L;

    public LibroException(HttpStatus errorCode, String errorMessage) {
        super("Libro", errorCode, errorMessage);
    }

    public LibroException() {
        super();
    }
}
