package cl.dev.jona.demolibros.common.util;

public class AutorConstant {
    public static final String AUTOR_NOT_FOUND_MESSAGE_ERROR = "No se encontró el autor con el id %s";
}
