package cl.dev.jona.demolibros.common.util;

import cl.dev.jona.demolibros.model.entity.Autor;
import net.sf.jasperreports.engine.*;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.export.JRXlsExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.stereotype.Service;
import org.springframework.util.ResourceUtils;

import org.apache.poi.xssf.usermodel.XSSFWorkbook;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class AutorGeneratorReport {

    public byte[] exportToPdf(List<Autor> list) throws JRException, FileNotFoundException {
        return JasperExportManager.exportReportToPdf(getReport(list));
    }

    public byte[] exportToXls(List<Autor> list) throws JRException, FileNotFoundException {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();
        SimpleOutputStreamExporterOutput output = new SimpleOutputStreamExporterOutput(byteArray);
        JRXlsExporter exporter = new JRXlsExporter();
        exporter.setExporterInput(new SimpleExporterInput(getReport(list)));
        exporter.setExporterOutput(output);
        exporter.exportReport();
        output.close();
        return byteArray.toByteArray();
    }

    public byte[] exportToXlsx(List<Autor> list) throws IOException {
        ByteArrayOutputStream byteArray = new ByteArrayOutputStream();

        // Crear un nuevo libro de trabajo XSSFWorkbook (XLSX)
        try (Workbook workbook = new XSSFWorkbook()) {
            // Crear una hoja de trabajo
            Sheet sheet = workbook.createSheet("Autores");

            // Crear el encabezado de la tabla
            Row headerRow = sheet.createRow(0);
            headerRow.createCell(0).setCellValue("ID");
            headerRow.createCell(1).setCellValue("Nombre");
            headerRow.createCell(2).setCellValue("Nacionalidad");
            headerRow.createCell(3).setCellValue("Fecha de Nacimiento");

            // Llenar los datos de la tabla
            int rowNum = 1;
            for (Autor autor : list) {
                Row row = sheet.createRow(rowNum++);
                row.createCell(0).setCellValue(autor.getId_autor());
                row.createCell(1).setCellValue(autor.getNombre());
                row.createCell(2).setCellValue(autor.getNacionalidad());
                // Asegúrate de tener el formato de fecha adecuado aquí
                row.createCell(3).setCellValue(autor.getFecha_nacimiento().toString());
            }

            // Ajustar automáticamente el ancho de las columnas
            for (int i = 0; i < 4; i++) {
                sheet.autoSizeColumn(i);
            }

            // Escribir el libro de trabajo en el flujo de salida
            workbook.write(byteArray);
        }

        return byteArray.toByteArray();
    }

    private JasperPrint getReport(List<Autor> list) throws FileNotFoundException, JRException {
        Map<String, Object> params = new HashMap<String, Object>();
        params.put("autorData", new JRBeanCollectionDataSource(list));

        JasperPrint report = JasperFillManager.fillReport(JasperCompileManager.compileReport(
                ResourceUtils.getFile("classpath:autorReport.jrxml")
                        .getAbsolutePath()), params, new JREmptyDataSource());

        return report;
    }
}
