package cl.dev.jona.demolibros.common.exception;

import org.springframework.http.HttpStatus;

public class AutorException extends EntityException {
    private static final long serialVersionUID = 1L;

    public AutorException(HttpStatus errorCode, String errorMessage) {
        super("Autor", errorCode, errorMessage);
    }

    public AutorException() {
        super();
    }
}
