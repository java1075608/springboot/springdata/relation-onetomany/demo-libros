package cl.dev.jona.demolibros.common.util;

public class LibroConstant {
    public static final String LIBRO_NOT_FOUND_MESSAGE_ERROR = "No se encontró el libro con el id %s";
}
