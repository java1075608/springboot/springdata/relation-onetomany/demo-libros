package cl.dev.jona.demolibros.controller.rest;


import cl.dev.jona.demolibros.model.entity.Autor;
import cl.dev.jona.demolibros.model.service.IAutorService;
import net.sf.jasperreports.engine.JRException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "http://localhost:4200/")
@RequestMapping("/api")
public class AutorController {
    @Autowired
    IAutorService autorService;


    @GetMapping("/autores")
    public List<Autor> listarAutores(){
        return autorService.findAll();
    }

    @GetMapping("/autores/{id_autor}")
    public Optional<Autor> getById(@PathVariable Long id_autor){
        return  autorService.findById(id_autor);
    }

    @GetMapping("/autores/buscar")
    public List<Autor> buscarAutoresPorPrimeraPalabra(@RequestParam("nombre") String nombre) {
        String primeraPalabra = nombre.trim().split(" ")[0];
        return autorService.findByNombreStartingWithIgnoreCase(primeraPalabra);
    }

    @GetMapping("/export-pdf")
    public ResponseEntity<byte[]> exportPdf() throws JRException, FileNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.setContentType(MediaType.APPLICATION_PDF);
        headers.setContentDispositionFormData("autorReport", "autorReport.pdf");
        return ResponseEntity.ok().headers(headers).body(autorService.exportPdf());
    }

    @GetMapping("/export-xls")
    public ResponseEntity<byte[]> exportXls() throws JRException, FileNotFoundException {
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
        var contentDisposition = ContentDisposition.builder("attachment")
                .filename("autorReport" + ".xls").build();
        headers.setContentDisposition(contentDisposition);
        return ResponseEntity.ok()
                .headers(headers)
                .body(autorService.exportXls());
    }

    @GetMapping("/export-xlsx")
    public ResponseEntity<byte[]> exportXlsx() throws IOException {
        byte[] fileContent = autorService.exportXlsx(); // Llama al método de exportación que definimos antes

        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-Type", "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet; charset=UTF-8");
        var contentDisposition = ContentDisposition.builder("attachment")
                .filename("autorReport.xlsx").build(); // Cambia la extensión a .xlsx
        headers.setContentDisposition(contentDisposition);

        return ResponseEntity.ok()
                .headers(headers)
                .body(fileContent);
    }


}
