package cl.dev.jona.demolibros.controller.advice;

import cl.dev.jona.demolibros.common.exception.EntityException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class ControllerAdvice {
    @ExceptionHandler(EntityException.class)
    public ResponseEntity<String> handleEntityException(EntityException entityException) {
        return new ResponseEntity<String>(entityException.getErrorMessage(), entityException.getErrorCode());
    }
}
