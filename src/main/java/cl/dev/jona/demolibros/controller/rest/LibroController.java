package cl.dev.jona.demolibros.controller.rest;

import cl.dev.jona.demolibros.model.entity.Libro;
import cl.dev.jona.demolibros.model.service.ILibroService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin
@RequestMapping("/api")
public class LibroController {
    @Autowired
    ILibroService libroService;

    @GetMapping("/libros")
    public List<Libro> listarLibros(){
        return libroService.listarLibros();
    }

    @GetMapping("/libros/{id_libro}")
    public Optional<Libro> getById(@PathVariable Long id_libro){
        return  libroService.findById(id_libro);
    }

}