package cl.dev.jona.demolibros;

import cl.dev.jona.demolibros.model.entity.Autor;
import cl.dev.jona.demolibros.model.entity.Libro;
import cl.dev.jona.demolibros.model.repository.AutorRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@SpringBootApplication
public class DemoLibrosApplication implements CommandLineRunner {

    @Autowired
    AutorRepository autorRepository;

    public static void main(String[] args) {
        SpringApplication.run(DemoLibrosApplication.class, args);
        System.out.println("hola mundo");
    }

    @Override
    public void run(String... args) throws Exception {
        String fechaNacimientoG = "01-03-1892";
        String fechaNacimientoT = "03-06-1927";
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
        Date fechaNacimientoGarcia = sdf.parse(fechaNacimientoG);
        Date fechaNacimientoTolkien = sdf.parse(fechaNacimientoT);
        Autor autor = new Autor("J.R.R. Tolkien", "tolkien12@gmail.com","Británico",fechaNacimientoTolkien,"https://i.ibb.co/RhvqJrP/Tolkien.jpg");
        Autor autor2 = new Autor("Gabriel García Márquez","g.marquz@hotmail.es","Colombiano", fechaNacimientoGarcia,"https://i.ibb.co/WyFGv9L/Gabriel.jpg");

        Libro l1 = new Libro("El Señor de los Anillos: La Comunidad del Anillo", "Primera edición", 1954);
        Libro l2 = new Libro(" El Señor de los Anillos: Las Dos Torres", "Primera edición", 1954);
        Libro l3 = new Libro("El Señor de los Anillos: El Retorno del Rey", "Primera edición", 1955);
        Libro l4 = new Libro("Cien años de soledad","Primera edición",1967);

        List<Libro> libros = new ArrayList<>();
        List<Libro> librosMarquez = new ArrayList<>();

        System.out.println(fechaNacimientoG);
        System.out.println(fechaNacimientoT);
		l1.setAutor(autor);
		l2.setAutor(autor);
		l3.setAutor(autor);
        l4.setAutor(autor2);

		libros.add(l1);
		libros.add(l2);
		libros.add(l3);
        librosMarquez.add(l4);


		autor.setLibros(libros);
        autor2.setLibros(librosMarquez);

        autorRepository.save(autor2);
		Autor autorSave = autorRepository.save(autor);

        System.out.println(autorSave.getNombre());
        autorSave.getLibros().forEach(x -> System.out.println("libros creados: "+x.getNombre()));




    }
}
