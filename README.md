# Gestión de libros
Este proyecto tiene un objetivo pedagógico para representar entidades en SpringBoot, específicamente una relación de 1 a N (**OneToMany**). 

## Requisitos

- Java 17
- Spring Boot 3
- PostgreSQL 13
- Visual Paradigm Community
- IntelliJ IDEA Community
- Arquitectura MVC

## Configuración del Proyecto

1. Clona el repositorio en tu máquina local.
2. Abre IntelliJ y selecciona "Import Project".
3. Navega hasta la ubicación del proyecto clonado y selecciona el archivo `pom.xml` para importar el proyecto como un proyecto Maven.
4. Configura las variables de entorno necesarias para la conexión a la base de datos PostgreSQL.
5. Abre el archivo `application.properties` y configura las propiedades de la base de datos de acuerdo a tu configuración.
6. Inicia la aplicación.


## Diagrama de clases del proyecto

![Descripción de la imagen](DC.PNG)

